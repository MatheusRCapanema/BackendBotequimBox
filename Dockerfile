## Build Stage
FROM maven:3.8.2-openjdk-17 as build
COPY . .
RUN mvn clean package -Pprod -DskipTests

## Package Stage
FROM openjdk:17-slim
COPY --from=build /target/BackendBotequimBox-0.0.1-SNAPSHOT.jar backendbotequimbox.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "backendbotequimbox.jar"]
