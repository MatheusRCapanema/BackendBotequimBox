package com.example.backendbotequimbox.dto.user;

import com.example.backendbotequimbox.entity.Role;
import com.example.backendbotequimbox.entity.User;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
public abstract class UserBaseDto implements Serializable {

    protected Long id;
    protected String cnpj;
    protected String email;
    protected String type;
    protected List<String> roles;
    protected LocalDateTime createdAt;
    protected LocalDateTime updatedAt;

    protected UserBaseDto(final User user) {
        id = user.getId();
        cnpj = user.getCnpj();
        email = user.getEmail();
        type = user.getType().getName();
        roles = getUserRolesToString(user.getRoles());
        createdAt = user.getCreatedAt();
        updatedAt = user.getUpdatedAt();
    }

    protected List<String> getUserRolesToString(List<Role> roles) {
        return roles.stream().map(Role::toString).collect(Collectors.toList());
    }
}
