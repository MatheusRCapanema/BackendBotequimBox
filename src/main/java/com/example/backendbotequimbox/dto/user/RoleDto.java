package com.example.backendbotequimbox.dto.user;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder(setterPrefix = "with")
public class RoleDto {

    private String name;
    private String description;

    public RoleDto(String name, String description) {
        this.name = name;
        this.description = description;
    }
}
