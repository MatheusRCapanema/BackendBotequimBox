package com.example.backendbotequimbox.dto.user;

import com.example.backendbotequimbox.enums.UserTypeEnum;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder(setterPrefix = "with")
public class UserTypeDto {

    private String name;
    private String description;

    public UserTypeDto(String name, String description) {
        this.name = name;
        this.description = description;
    }
}
