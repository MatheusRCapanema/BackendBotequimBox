package com.example.backendbotequimbox.dto.user;

import com.example.backendbotequimbox.entity.User;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserDto extends UserBaseDto {

    public UserDto(User user) {
        super(user);
    }
}
