package com.example.backendbotequimbox.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum UserTypeEnum {

    TYPE_MANUFACTURER("TYPE_MANUFACTURER", "TYPE_MANUFACTURER"),
    TYPE_RETAILER("TYPE_RETAILER", "TYPE_RETAILER");

    private final String name;
    private final String description;
}
