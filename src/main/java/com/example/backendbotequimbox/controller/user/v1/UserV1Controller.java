package com.example.backendbotequimbox.controller.user.v1;

import com.example.backendbotequimbox.dto.user.RoleDto;
import com.example.backendbotequimbox.dto.user.UserTypeDto;
import com.example.backendbotequimbox.enums.UserTypeEnum;
import com.example.backendbotequimbox.service.user.RoleService;
import com.example.backendbotequimbox.service.user.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/api/v1/users")
public class UserV1Controller {

    private final UserService userService;
    private final RoleService roleService;

    @GetMapping(path = "/types")
    public ResponseEntity<List<UserTypeDto>> userTypes() {
        return new ResponseEntity<>(userService.userTypes(), HttpStatus.OK);
    }

    @GetMapping(path = "/roles")
    public ResponseEntity<List<RoleDto>> roles() {
        return new ResponseEntity<>(roleService.getRoleList(), HttpStatus.OK);
    }
}
