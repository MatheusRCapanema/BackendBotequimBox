package com.example.backendbotequimbox.repository;

import com.example.backendbotequimbox.entity.Role;
import com.example.backendbotequimbox.enums.UserRoleEnum;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Long> {

    Optional<Role> findByName(UserRoleEnum name);
}