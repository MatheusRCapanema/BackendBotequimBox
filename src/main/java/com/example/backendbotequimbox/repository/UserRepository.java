package com.example.backendbotequimbox.repository;

import com.example.backendbotequimbox.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByCnpj(String cnpj);

    Optional<User> findByEmailIgnoreCase(String email);

    @Query(value = "SELECT u FROM User u WHERE u.cnpj = ?1 OR u.email = ?1")
    Optional<User> findByCnpjOrEmail(String value);

    Boolean existsByCnpj(String cnpj);

    Boolean existsByEmailIgnoreCase(String email);
}