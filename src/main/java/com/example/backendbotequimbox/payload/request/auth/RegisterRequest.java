package com.example.backendbotequimbox.payload.request.auth;

import com.example.backendbotequimbox.enums.UserTypeEnum;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RegisterRequest {

    private String cnpj;
    private String email;
    private String password;
    private UserTypeEnum type;
}
