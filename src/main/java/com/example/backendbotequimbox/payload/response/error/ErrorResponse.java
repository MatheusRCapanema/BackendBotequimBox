package com.example.backendbotequimbox.payload.response.error;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder(setterPrefix = "with")
public class ErrorResponse {

    private int statusCode;
    private String error;
    private String message;
    private String path;
    private Object details;
    private String timestamp;
}