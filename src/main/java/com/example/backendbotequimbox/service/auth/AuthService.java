package com.example.backendbotequimbox.service.auth;

import com.example.backendbotequimbox.dto.user.UserDto;
import com.example.backendbotequimbox.exception.user.UserAlreadyExistsException;
import com.example.backendbotequimbox.payload.request.auth.AuthenticationRequest;
import com.example.backendbotequimbox.payload.request.auth.RegisterRequest;
import com.example.backendbotequimbox.payload.response.auth.AuthenticationResponse;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

public interface AuthService {

    UserDto register(RegisterRequest request) throws UserAlreadyExistsException;

    AuthenticationResponse authenticate(AuthenticationRequest authRequest);

    void refreshToken(HttpServletRequest request, HttpServletResponse response) throws IOException;
}