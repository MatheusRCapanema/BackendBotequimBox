package com.example.backendbotequimbox.service.auth;

import com.example.backendbotequimbox.dto.user.UserDto;
import com.example.backendbotequimbox.entity.User;
import com.example.backendbotequimbox.exception.user.UserAlreadyExistsException;
import com.example.backendbotequimbox.payload.request.auth.AuthenticationRequest;
import com.example.backendbotequimbox.payload.request.auth.RegisterRequest;
import com.example.backendbotequimbox.payload.response.auth.AuthenticationResponse;
import com.example.backendbotequimbox.repository.UserRepository;
import com.example.backendbotequimbox.service.jwt.JwtService;
import com.example.backendbotequimbox.service.user.RoleService;
import com.example.backendbotequimbox.service.user.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.Clock;
import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class AuthServiceImpl implements AuthService {

    private final Clock clock;
    private final JwtService jwtService;
    private final UserService userService;
    private final RoleService roleService;
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final UserDetailsService userDetailsService;
    private final AuthenticationManager authenticationManager;

    @Override
    public UserDto register(RegisterRequest request) throws UserAlreadyExistsException {

        checkIfUserExistsByCnpjAndThrowException(request.getCnpj());
        checkIfUserExistsByEmailAndThrowException(request.getEmail().trim().toLowerCase());

        final User user = User.builder()
                .withCnpj(request.getCnpj())
                .withEmail(request.getEmail().trim().toLowerCase())
                .withPassword(getPasswordHashed(request.getPassword().trim()))
                .withType(request.getType())
                .withRoles(roleService.getUserDefaultRoles(null))
                .withIsEnabled(true)
                .withIsAccountNonExpired(true)
                .withIsAccountNonLocked(true)
                .withIsCredentialsNonExpired(true)
                .withCreatedAt(LocalDateTime.now(clock))
                .withUpdatedAt(null)
                .build();

        return new UserDto(userRepository.save(user));
    }

    public AuthenticationResponse authenticate(AuthenticationRequest authRequest) {
        final Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(authRequest.getUsername(), authRequest.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);

        final User user = (User) userDetailsService.loadUserByUsername(authRequest.getUsername());

        final String accessToken = jwtService.generateAccessToken(user);
        final String refreshToken = jwtService.generateRefreshToken(user);

        return AuthenticationResponse.builder()
                .withId(user.getId())
                .withCnpj(user.getCnpj())
                .withEmail(user.getEmail())
                .withType(user.getType().getName())
                .withRoles(roleService.getRolesToString(user.getRoles()))
                .withAccessToken(accessToken)
                .withRefreshToken(refreshToken)
                .build();
    }

    public void refreshToken(HttpServletRequest request, HttpServletResponse response) throws IOException {
        final String authHeader = request.getHeader(HttpHeaders.AUTHORIZATION);
        final String refreshToken;
        final String username;

        if (authHeader == null || !authHeader.startsWith("Bearer ")) {
            return;
        }

        refreshToken = authHeader.substring(7);
        username = jwtService.extractUsername(refreshToken);

        if (username == null) {
            return;
        }

        User user = (User) userDetailsService.loadUserByUsername(username);

        if (jwtService.isTokenValid(refreshToken, user)) {
            final String accessToken = jwtService.generateAccessToken(user);
            final AuthenticationResponse authResponse = AuthenticationResponse.builder()
                    .withId(user.getId())
                    .withCnpj(user.getCnpj())
                    .withEmail(user.getEmail())
                    .withType(user.getType().getName())
                    .withRoles(roleService.getRolesToString(user.getRoles()))
                    .withAccessToken(accessToken)
                    .withRefreshToken(refreshToken)
                    .build();

            new ObjectMapper().writeValue(response.getOutputStream(), authResponse);
        }
    }

    private void checkIfUserExistsByCnpjAndThrowException(final String cnpj) {
        if (userService.existsUserByCnpj(cnpj)) {
            throw new UserAlreadyExistsException("Cnpj", cnpj);
        }
    }

    private void checkIfUserExistsByEmailAndThrowException(final String email) {
        if (userService.existsUserByEmail(email)) {
            throw new UserAlreadyExistsException("Email", email);
        }
    }

    private String getPasswordHashed(final String plainPassword) {
        return passwordEncoder.encode(plainPassword);
    }
}
