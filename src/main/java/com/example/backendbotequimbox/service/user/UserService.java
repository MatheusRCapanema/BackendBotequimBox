package com.example.backendbotequimbox.service.user;

import com.example.backendbotequimbox.dto.user.UserDto;
import com.example.backendbotequimbox.dto.user.UserTypeDto;
import com.example.backendbotequimbox.exception.user.UserNotFoundException;

import java.util.List;

public interface UserService {

    void enableUser(Long id) throws UserNotFoundException;

    Boolean existsUserByEmail(String email);

    Boolean existsUserById(Long id);

    Boolean existsUserByCnpj(String cnpj);

    UserDto findById(Long id) throws UserNotFoundException;

    UserDto findByCnpjOrEmail(String value) throws UserNotFoundException;

    List<UserTypeDto> userTypes();
}
