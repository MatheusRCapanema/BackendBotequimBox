package com.example.backendbotequimbox.service.user;

import com.example.backendbotequimbox.dto.user.UserDto;
import com.example.backendbotequimbox.dto.user.UserTypeDto;
import com.example.backendbotequimbox.entity.User;
import com.example.backendbotequimbox.enums.UserTypeEnum;
import com.example.backendbotequimbox.exception.user.UserNotFoundException;
import com.example.backendbotequimbox.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.Clock;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final Clock clock;
    private final UserRepository userRepository;

    @Override
    public void enableUser(Long id) throws UserNotFoundException {
        User user = userRepository.findById(id).orElseThrow(() -> new UserNotFoundException("Id", id));

        user.setIsEnabled(true);
        user.setUpdatedAt(LocalDateTime.now(clock));

        userRepository.save(user);
    }

    @Override
    public Boolean existsUserByEmail(String email) {
        return userRepository.existsByEmailIgnoreCase(email);
    }

    @Override
    public Boolean existsUserById(Long id) {
        return userRepository.existsById(id);
    }

    @Override
    public Boolean existsUserByCnpj(String cnpj) {
        return userRepository.existsByCnpj(cnpj);
    }

    @Override
    public UserDto findById(Long id) throws UserNotFoundException {
        return new UserDto(userRepository.findById(id)
                .orElseThrow(() -> new UserNotFoundException("Id", id)));
    }

    @Override
    public UserDto findByCnpjOrEmail(String value) throws UserNotFoundException {
        return new UserDto(userRepository.findByCnpjOrEmail(value)
                .orElseThrow(() -> new UserNotFoundException("Value", value)));
    }

    @Override
    public List<UserTypeDto> userTypes() {
        return Arrays.stream(UserTypeEnum.values())
                .map(typeEnum -> new UserTypeDto(typeEnum.getName(), typeEnum.getDescription()))
                .collect(Collectors.toList());
    }
}
