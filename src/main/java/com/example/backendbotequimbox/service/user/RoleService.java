package com.example.backendbotequimbox.service.user;

import com.example.backendbotequimbox.dto.user.RoleDto;
import com.example.backendbotequimbox.entity.Role;
import com.example.backendbotequimbox.enums.UserRoleEnum;

import java.util.List;

public interface RoleService {

    Role saveRole(Role role);

    Role getOrSaveRoleByName(UserRoleEnum name);

    List<Role> getUserDefaultRoles(List<Role> userRoles);

    List<String> getRolesToString(List<Role> userRoles);

    List<RoleDto> getRoleList();
}
