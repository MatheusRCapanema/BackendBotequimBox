package com.example.backendbotequimbox.service.user;

import com.example.backendbotequimbox.exception.auth.AuthBadCredentialsException;
import com.example.backendbotequimbox.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findByCnpjOrEmail(username).orElseThrow(AuthBadCredentialsException::new);
    }
}
