package com.example.backendbotequimbox.service.user;

import com.example.backendbotequimbox.dto.user.RoleDto;
import com.example.backendbotequimbox.entity.Role;
import com.example.backendbotequimbox.enums.UserRoleEnum;
import com.example.backendbotequimbox.repository.RoleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class RoleServiceImpl implements RoleService {

    private final RoleRepository roleRepository;

    @Override
    public Role saveRole(Role role) {
        return getOrSaveRoleByName(role.getName());
    }

    @Override
    public Role getOrSaveRoleByName(UserRoleEnum name) {
        return roleRepository.findByName(name).orElseGet(() -> roleRepository.save(new Role(name)));
    }

    public List<Role> getUserDefaultRoles(List<Role> userRoles) {
        List<Role> roles = new ArrayList<>();
        Role role = getOrSaveRoleByName(UserRoleEnum.ROLE_USER);
        roles.add(role);

        if (userRoles == null || userRoles.isEmpty()) {
            return roles;
        }

        return userRoles.stream()
                .map(uRole -> getOrSaveRoleByName(uRole.getName()))
                .collect(Collectors.toList());
    }

    public List<String> getRolesToString(List<Role> userRoles) {
        return userRoles.stream()
                .map(role -> role.getName().getName())
                .collect(Collectors.toList());
    }

    @Override
    public List<RoleDto> getRoleList() {
        return Arrays.stream(UserRoleEnum.values())
                .map(roleEnum -> new RoleDto(roleEnum.getName(), roleEnum.getDescription()))
                .collect(Collectors.toList());
    }
}
