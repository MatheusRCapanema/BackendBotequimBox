package com.example.backendbotequimbox.entity;

import com.example.backendbotequimbox.enums.UserRoleEnum;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "tab_roles",
        uniqueConstraints = {@UniqueConstraint(name = "role_name_uk", columnNames = {"name"})},
        indexes = {@Index(name = "role_name_idx", columnList = "name", unique = true)}
)
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(nullable = false, updatable = false)
    private Long id;

    @Column(nullable = false, unique = true)
    @Enumerated(value = EnumType.STRING)
    private UserRoleEnum name;

    public Role(UserRoleEnum name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name.getName();
    }
}
