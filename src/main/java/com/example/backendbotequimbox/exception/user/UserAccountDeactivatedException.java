package com.example.backendbotequimbox.exception.user;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class UserAccountDeactivatedException extends RuntimeException {

    public UserAccountDeactivatedException(String fieldName, String fieldValue) {
        super("User Account deactivated.");
    }

    public UserAccountDeactivatedException(String message) {
        super(message);
    }
}
