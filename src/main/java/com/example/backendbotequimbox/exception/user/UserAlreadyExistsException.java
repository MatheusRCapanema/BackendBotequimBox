package com.example.backendbotequimbox.exception.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class UserAlreadyExistsException extends RuntimeException {

    private String fieldName;
    private Object fieldValue;

    public UserAlreadyExistsException(String fieldName, Object fieldValue) {
        super(String.format("User Already Exists with this %s : %s", fieldName, fieldValue));
        this.fieldName = fieldName;
        this.fieldValue = fieldValue;
    }

    public UserAlreadyExistsException(String message) {
        super(message);
    }
}
