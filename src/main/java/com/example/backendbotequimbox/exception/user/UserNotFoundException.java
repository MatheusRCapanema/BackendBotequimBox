package com.example.backendbotequimbox.exception.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class UserNotFoundException extends RuntimeException {

    private String fieldName;
    private Object fieldValue;

    public UserNotFoundException(String fieldName, Object fieldValue) {
        super(String.format("User Not Found with this %s : %s", fieldName, fieldValue));
        this.fieldName = fieldName;
        this.fieldValue = fieldValue;
    }

    public UserNotFoundException(String message) {
        super(message);
    }
}
